#sudo systemctl start docker

# Compile
# with docker

docker run -it --rm --name app -v "$(pwd)":/home/ubuntu/work/jaee/app/ -w /home/ubuntu/work/jaee/app/ maven:3.8-jdk-11 mvn install

# error: 
# run maven persistence
# docker run -it -v maven-repo:/root/.m2 maven mvn archetype:generate
# docker run -it --rm -v "$PWD":/home/bazil/work/jaee/app/ -v "$HOME/.m2":/root/.m2 -v "$PWD/target:/home/bazil/work/jaee/app/target" -w /home/bazil/work/jaee/app/ app mvn clean package  

# WildFly
cp ./target/app.war ./
docker build --tag=app .
docker run -it app

# Output
# Run in another terminal
# docker inspect -f '{{ .NetworkSettings.IPAddress }}' b63e7345bf81
# curl  http://172.17.0.2:8080/app/hello.xhtml
