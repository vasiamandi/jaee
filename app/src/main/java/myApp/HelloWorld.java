package myApp;

import java.util.Calendar;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path ("forecast")
public class HelloWorld {
	@GET
	public String getForecast() {
		return "This is the time: " + Calendar.getInstance().getTime();
	}

}
